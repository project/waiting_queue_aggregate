<?php

/**
 * Exception on claiming an item from a subqueue.
 */
class WaitingQueueAggregateQueueclaimItemFromSubqueuesException extends Exception {

}
