<?php

namespace Drupal\waiting_queue_aggregate\Queue;

class WaitingQueueAggregateQueueFactory  {
  /**
   * Constructs a new queue object for a given name.
   *
   * @param string $name
   *   The name of the queue to construct.
   *
   * @return WaitingQueueAggregateQueue
   *   A key/value store implementation for the given $collection.
   */
  public function get($name) {
    if ($name !== 'waiting_queue_aggregate') {
      throw new \InvalidArgumentException('The queue name must be "waiting_queue_aggregate".');
    }
    // Get all the names of the configured subqueues.
    $config = \Drupal::config('waiting_queue_aggregate.settings');
    return new WaitingQueueAggregateQueue($config->get('enabled_queues') ?? []);
  }

}
