<?php

namespace Drupal\waiting_queue_aggregate\Queue;

use Drupal\Core\Queue\QueueGarbageCollectionInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\ReliableQueueInterface;
use Drupal\waiting_queue_aggregate\WaitingQueueAggregateWrappedItem;
use WaitingQueueAggregateQueueclaimItemFromSubqueuesException;
use WaitingQueueAggregateQueuecreateItemException;

/**
 * Waiting Queue Aggregate Queue class.
 */
final class WaitingQueueAggregateQueue implements ReliableQueueInterface, QueueGarbageCollectionInterface {

  /**
   * An array of subqueue names that this class should aggregate from.
   */
  protected $subQueueNames;

  /**
   * An array of subqueue instances that this class is aggregating.
   */
  protected $subQueueInstances;

  /**
   * A list of sub queue names this class is working through.
   *
   * @var string[]
   */
  protected $subQueuesToCheckForItems;

  /**
   * A list of sub queues this class currently considers empty.
   *
   * @var string[]
   */
  protected $subQueuesWithoutItems;

  /**
   * @param $subQueueNames
   */
  public function __construct(array $subQueueNames = []) {
    $this->subQueueNames = $subQueueNames;
  }

  /**
   * @inheritdoc
   */
  public function createItem($data) {
    throw new WaitingQueueAggregateQueuecreateItemException('Adding items to this queue is forbidden.');
  }

  /**
   * @inheritdoc
   */
  public function createQueue() {
    // We do not store items directly, so nothing to do here.
  }

  /**
   * @inheritdoc
   */
  public function deleteQueue() {
    // We do not store items directly, so nothing to do here.
  }

  /**
   * @inheritdoc
   */
  public function numberOfItems() {
    $items = 0;
    foreach ($this->getSubqueues() as $queue) {
      $items += $queue->numberOfItems();
    }
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function garbageCollection() {
    foreach ($this->getSubqueues() as $queue) {
      if ($queue instanceof QueueGarbageCollectionInterface) {
        $queue->garbageCollection();
      }
    }
  }

  /**
   * Try and claim an item from the subqueues.
   *
   * @param int $lease_time
   *   The time to claim an item for.
   *
   * @return mixed
   *   FALSE if no item could be claimed, or an item if it could.
   */
  public function claimItem($lease_time = 3600) {
    $this->ensureStartStats();
    return $this->claimItemFromSubqueues($lease_time);
  }

  /**
   * @inheritdoc
   */
  public function deleteItem($item) {
    if (!$item instanceof WaitingQueueAggregateWrappedItem) {
      throw new \InvalidArgumentException('The item to delete must be an instance of WaitingQueueAggregateWrappedItem.');
    }
    $queue = $this->getSubqueue($item->getQueueName());
    $queue->deleteItem($item->getItem());
  }

  /**
   * @inheritdoc
   */
  public function releaseItem($item) {
    if (!$item instanceof WaitingQueueAggregateWrappedItem) {
      throw new \InvalidArgumentException('The item to delete must be an instance of WaitingQueueAggregateWrappedItem.');
    }
    $queue = $this->getSubqueue($item->getQueueName());
    $queue->releaseItem($item->getItem());
  }

  /**
   * Claim a single item from one of the configured subqueues.
   *
   * @param int $lease_time
   *   The time to claim an item for.
   *
   * @return bool|WaitingQueueAggregateWrappedItem
   *   Either FALSE if no item could be claimed, or the claimed item.
   *
   * @throws WaitingQueueAggregateQueueclaimItemFromSubqueuesException
   */
  protected function claimItemFromSubqueues($lease_time = 900) {
    // Set up the queues to work through.
    if (empty($this->subQueuesToCheckForItems)) {
      // Reset to having no queues without items.
      $this->subQueuesWithoutItems = array();

      // Create a list of sub queue names, duplicated 25 times over.
      // This is so that when we shuffle the list, we have a better chance of
      // doing lots of work before deciding to shuffle again.
      foreach (array_keys($this->getSubqueues()) as $subqueue_name) {
        for ($i = 0; $i < 25; $i++) {
          $this->subQueuesToCheckForItems[] = $subqueue_name;
        }
      }

      // Shuffle the possible work into random order.
      shuffle($this->subQueuesToCheckForItems);

      if (empty($this->subQueuesToCheckForItems)) {
        throw new WaitingQueueAggregateQueueclaimItemFromSubqueuesException('No subqueues configured for processing.');
      }
    }

    // Done setup, now try to get the next item to work through.
    while ($subqueue_name = array_pop($this->subQueuesToCheckForItems)) {
      if (!in_array($subqueue_name, $this->subQueuesWithoutItems, TRUE)) {
        if ($item = $this->getSubqueue($subqueue_name)
          ->claimItem($lease_time)) {
          // Return a sub queue item, wrapped to include the sub queue name.
          return $this->wrapItem($subqueue_name, $item);
        }
        else {
          // No work in this sub queue for the moment.
          $this->subQueuesWithoutItems[] = $subqueue_name;
        }
      }
    }

    return FALSE;
  }

  /**
   * Wrap a subqueue item.
   *
   * @param string $queue
   *   The subqueue name.
   * @param mixed $item
   *   The subqueue item.
   *
   * @return WaitingQueueAggregateWrappedItem
   *   The wrapped subqueue item.
   */
  protected function wrapItem($queue, $item) {
    $this->statsRecordItemProcessed();
    return new WaitingQueueAggregateWrappedItem($queue, $item);
  }

  /**
   * Get an array of all configured subqueues.
   *
   * @return QueueInterface[]
   *   The array of configured subqueues.
   */
  protected function getSubqueues() {
    if (!isset($this->subQueueInstances)) {
      $this->subQueueInstances = array();
      foreach ($this->subQueueNames as $subQueueName) {
        $this->subQueueInstances[$subQueueName] = \Drupal::queue($subQueueName);
      }
    }
    return $this->subQueueInstances;
  }

  /**
   * Get a given subqueue.
   *
   * @param string $name
   *   The name of the queue to get the queue class for.
   *
   * @return QueueInterface
   *   The queue.
   */
  protected function getSubqueue($name) {
    $queues = $this->getSubqueues();
    return $queues[$name] ?? NULL;
  }

  protected $startTime;
  protected $itemsProcessed = 0;
  protected $processID;

  /**
   * Ensure that the statistics collection has started.
   */
  protected function ensureStartStats() {
    if (is_null($this->startTime)) {
      $this->startTime = time();
      $this->processID = uniqid(mt_rand(), TRUE);
    }
  }

  /**
   * Write our stats to the database.
   */
  protected function writeStats($isTerminating = FALSE) {
    if (!is_null($this->startTime)) {
      \Drupal::database()->merge('waiting_queue_aggregate_stats')
        ->key('process_id', $this->processID)
        ->fields(array(
          'start_time' => $this->startTime,
          'items_processed' => $this->itemsProcessed,
          'last_seen_time' => time(),
          'status' => $isTerminating ? t('Terminated') : t('Processing'),
        ))
        ->execute();
    }
  }

  /**
   * Destructor to write the stats at the end.
   */
  public function __destruct() {
    // @TODO: This blew up because the database wasn't around any more. Try to bring it back somehow? Maybe a plain shutdown function?
    // $this->writeStats(TRUE);
  }

  /**
   * Record that an item was processed.
   */
  protected function statsRecordItemProcessed() {
    $this->itemsProcessed++;

    if ($this->itemsProcessed > 0 && $this->itemsProcessed % 10 == 1) {
      $this->writeStats();
    }
  }

}
