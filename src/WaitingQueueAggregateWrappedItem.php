<?php

namespace Drupal\waiting_queue_aggregate;

class WaitingQueueAggregateWrappedItem {

  protected $item;

  protected $queue_name;

  /**
   * Drupal likes to access ->data directly on our queue item.
   *
   * But we really want to keep a reference to ourselves around, so we set up a
   * circular reference.
   *
   * @var \Drupal\waiting_queue_aggregate\WaitingQueueAggregateWrappedItem
   */
  public $data;

  public function __construct($queue_name, $item) {
    $this->item = $item;
    $this->queue_name = $queue_name;
    // Super sneaky circular reference.
    $this->data = $this;
  }

  /**
   * @return mixed
   */
  public function getItem() {
    return $this->item;
  }

  /**
   * @return mixed
   */
  public function getQueueName() {
    return $this->queue_name;
  }

}
