<?php

namespace Drupal\waiting_queue_aggregate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Waiting Queue Aggregate settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'waiting_queue_aggregate_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['waiting_queue_aggregate.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = array();
    $config = $this->config('waiting_queue_aggregate.settings');

    // Get some STATS.
    $processes = \Drupal::database()->select('waiting_queue_aggregate_stats', 's')
      ->fields('s', array('last_seen_time', 'start_time', 'items_processed', 'status'))
      // Get stats from the two minutes.
      ->condition('s.last_seen_time', \Drupal::time()->getRequestTime() - 120, '>')
      ->orderBy('s.start_time', 'DESC')
      ->orderBy('s.process_id')
      ->range(0, 10)
      ->execute();

    $form['process'] = array(
      '#caption' => t('Processing Statistics'),
      '#theme' => 'table',
      '#header' => array(
        t('Started'),
        t('Status'),
        t('Last seen'),
        t('Items processed'),
      ),
      '#rows' => array(),
      '#empty' => t('No processes observed in the two minutes.'),
      '#prefix' => '<div id="waiting-queue-aggregate-statistics-wrapper">',
      '#suffix' => '</div>',
    );

    foreach ($processes as $process) {
      $lifetime = $process->last_seen_time - $process->start_time;
      $total_items = $process->items_processed;
      if ($lifetime > 0) {
        $avg_items = ' (' . number_format($total_items / $lifetime, 2) . ' items/sec)';
      }
      else {
        $avg_items = '';
      }
      $form['process']['#rows'][] = array(
        \Drupal::service('date.formatter')->formatInterval(\Drupal::time()->getRequestTime() - $process->start_time) . ' ago',
        $process->status,
        \Drupal::service('date.formatter')->formatInterval(\Drupal::time()->getRequestTime() - $process->last_seen_time) . ' ago',
        number_format($total_items) . $avg_items,
      );
    }

    $form['statistics_update'] = array(
      '#type' => 'submit',
      '#ajax' => array(
        'callback' => '::ajaxCallback',
        'wrapper' => 'waiting-queue-aggregate-statistics-wrapper',
        'method' => 'replace',
        'speed' => 'fast',
        'progress' => 'none',
      ),
      '#value' => t('Update statistics'),
      '#submit' => array(
        '::dummySubmit',
      ),
      '#attributes' => array(
        'class' => array(
          'js-waiting-queue-aggregate-statistics-update',
        )
      ),
      '#attached' => array(
        'library' => array(
          'waiting_queue_aggregate/admin',
        ),
      ),
    );


    // The actual settings.

    foreach (\Drupal::service('plugin.manager.queue_worker')->getDefinitions() as $queue_name => $info) {
      $options[$queue_name]['queue'] = $info['title'] ? t('@title (@machine_name)', [
        '@title' => $info['title'],
        '@machine_name' => $queue_name,
      ]) : $queue_name;
      $queue = \Drupal::queue($queue_name);
      $options[$queue_name]['count'] = number_format($queue->numberOfItems());
    }

    unset($options['waiting_queue_aggregate']);

    // Sort the selected options to the 'top'.
    $default_value = $config->get('enabled_queues') ?? [];
    $sorted_options = array();

    foreach ($default_value as $value) {
      if (isset($options[$value])) {
        $sorted_options[$value] = $options[$value];
        unset($options[$value]);
      }
    }
    foreach ($options as $name => $value) {
      $sorted_options[$name] = $value;
    }

    $form['enabled_queues'] = array(
      '#type' => 'tableselect',
      '#header' => array(
        'queue' => t('Queue'),
        'count' => t('Number of items'),
      ),
      '#options' => $sorted_options,
      '#multiple' => TRUE,
      '#default_value' => array_combine($default_value, $default_value),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('waiting_queue_aggregate.settings')
      ->set('enabled_queues', array_values(array_filter($form_state->getValue('enabled_queues'))))
      ->save();
    parent::submitForm($form, $form_state);
  }

  public function dummySubmit(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    return $form['process'];
  }

}
