<?php

/**
 * Exception on trying to add an item to the aggregate queue.
 */
class WaitingQueueAggregateQueuecreateItemException extends Exception {

}
