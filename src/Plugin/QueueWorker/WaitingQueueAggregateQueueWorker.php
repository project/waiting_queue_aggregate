<?php

namespace Drupal\waiting_queue_aggregate\Plugin\QueueWorker;

use Drupal\Core\Annotation\QueueWorker;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\QueueWorkerInterface;
use Drupal\waiting_queue_aggregate\WaitingQueueAggregateWrappedItem;
use WaitingQueueAggregateQueueprocessItemException;

/**
 * Queue worker for WaitingQueueAggregate.
 *
 * @QueueWorker(
 *   id = "waiting_queue_aggregate",
 *   title = @Translation("Process aggregated queue items"),
 * )
 */
final class WaitingQueueAggregateQueueWorker extends QueueWorkerBase {

  protected $subqueueWorkers = [];

  public function processItem($data) {
    if (!$data instanceof WaitingQueueAggregateWrappedItem) {
      throw new \InvalidArgumentException('The item to process must be an instance of WaitingQueueAggregateWrappedItem.');
    }
    $inner_item = $data->getItem();
    $this->getSubqueueWorker($data->getQueueName())->processItem($inner_item->data);
  }

  /**
   * Get the queue worker for a subqueue.
   *
   * @param $subqueue
   *   The subqueue to get the worker for.
   *
   * @return QueueWorkerInterface
   */
  protected function getSubqueueWorker($subqueue) {
    if (!isset($this->subqueueWorkers[$subqueue])) {
      $instance = \Drupal::service('plugin.manager.queue_worker')->createInstance($subqueue);
      if (empty($instance)) {
        throw new WaitingQueueAggregateQueueprocessItemException('Could not find queue callback for: ' . $subqueue);
      }
      $this->subqueueWorkers[$subqueue] = $instance;
    }

    return $this->subqueueWorkers[$subqueue];
  }

}
