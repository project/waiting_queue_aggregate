(function($) {

  Drupal.behaviors.waiting_queue_aggregate_statistics = {
    attach: function(context) {
      $(context).find('.js-waiting-queue-aggregate-statistics-update').once('waiting_queue_aggregate_statistics').each(function() {
        var $button = $(this);
        $button.hide();
        var pressButton = function() {
          $button.trigger('mousedown');
        };
        // Update every 5 seconds.
        setInterval(pressButton, 5000);
      });
    }
  }
})(jQuery);
